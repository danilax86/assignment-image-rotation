//
// Created by danila on 12/27/21.
//
#include "input_format.h"
#include <stdio.h>


#define BF_TYPE 0x4d42
#define BI_BIT_COUNT 24
#define BI_SIZE 40
#define DEFAULT_OFFSET 54

static inline uint32_t get_padding(uint32_t width) {
    return (4 - ((width * 3) % 4)) % 4;
}

enum read_status read_bmp_bits(FILE *file, struct image *image, struct bmp_header const *bmp) {
    uint32_t padding = get_padding(image->width);
    fseek(file, bmp->bOffBits, SEEK_SET);

    for (uint32_t y = 0; y < image->height; ++y) {
        size_t bits_read_res = fread(&image->data[image->width * y], sizeof(struct pixel), image->width, file);

        if (bits_read_res != image->width) {
            return READ_INVALID_BITS;
        }

        if (padding != 0 && fseek(file, padding, 1) != 0) {
            return READ_INVALID_BITS;
        }
    }

    return READ_OK;
}

enum read_status read_bmp_header(FILE *file, struct bmp_header *bmp) {
    fread(bmp, sizeof(struct bmp_header), 1, file);

    if (bmp->bfType > BF_TYPE || bmp->bfType <= 0) return READ_INVALID_SIGNATURE;
    if (bmp->biBitCount != BI_BIT_COUNT) return READ_INVALID_BITS;
    if (bmp->biSize != BI_SIZE) return READ_INVALID_HEADER;

    return READ_OK;
}


enum read_status from_bmp(char const *filename, struct image *image) {
    if (!filename) return READ_FILENAME_NOT_FOUND;
    FILE *file = fopen(filename, "rb");
    if (!file) return READ_FILE_ERROR;

    struct bmp_header bmp;
    enum read_status read_header_res = read_bmp_header(file, &bmp);
    if (read_header_res != READ_OK) {
        return read_header_res;
    }

    image_init(image, bmp.biHeight, bmp.biWidth);

    return read_bmp_bits(file, image, &bmp);
}


enum write_status write_bmp_header(FILE *file, struct image const *image) {
    if (!file) return WRITE_FILE_ERROR;

    struct bmp_header bmp = {
            .bfType = BF_TYPE,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = BI_SIZE,
            .biHeight = image->height,
            .biWidth = image->width,
            .biPlanes = 1,
            .biBitCount = BI_BIT_COUNT,
            .biSizeImage = BI_SIZE,
    };

    bmp.bfileSize = bmp.biSizeImage + sizeof(struct bmp_header);
    size_t header_write_res = fwrite(&bmp, sizeof(struct bmp_header), 1, file);

    if (header_write_res != 1) {
        return WRITE_FILE_ERROR;
    }

    return WRITE_OK;
}

enum write_status write_bmp_bits(FILE *file, struct image const *image) {
    if (ftell(file) < DEFAULT_OFFSET) return WRITE_FILE_ERROR;

    uint32_t padding = get_padding(image->width);

    static const char zeroes[] = {0, 0, 0, 0};
    for (uint32_t y = 0; y < image->height; y++) {
        size_t bits_write_res = fwrite(&image->data[image->width * y], sizeof(struct pixel), image->width, file);

        if (bits_write_res != image->width) {
            return WRITE_FILE_ERROR;
        }

        if (padding != 0 && fwrite(zeroes, sizeof(char), padding, file) != padding) {
            return WRITE_FILE_ERROR;
        }
    }

    return WRITE_OK;
}


enum write_status to_bmp(char const *filename, struct image const *image) {
    if (!filename) return WRITE_FILENAME_NOT_FOUND;
    if (!image) return WRITE_IMAGE_NOT_FOUND;

    FILE *file = fopen(filename, "wb");

    enum write_status header_write_res = write_bmp_header(file, image);
    if (header_write_res != WRITE_OK) {
        return WRITE_FILE_ERROR;
    }

    return write_bmp_bits(file, image);
}
