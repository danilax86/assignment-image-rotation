#include "input_format.h"
#include "transform.h"

#include <stdio.h>

int main(int argc, char **argv) {
    if (argc != 2 && argc != 3) {
        puts("Arguments: [input file] [output file](optional)");
        return 1;
    }

    const char *input_path = argv[1];
    const char *output_path;
    if (argc == 2) {
        output_path = "out.bmp";
    } else {
        output_path = argv[2];
    }

    int failure = 1;

    struct image in_image = {0};
    struct image out_image = {0};

    enum read_status result_from_bmp = from_bmp(input_path, &in_image);
    switch (result_from_bmp) {
        case READ_OK: {
            puts("Image is loaded.");
            break;
        }
        case READ_FILENAME_NOT_FOUND: {
            puts("Input file name is not specified");
            goto FREE_IMAGES;
        }
        case READ_FILE_ERROR: {
            puts("Unable to open input file");
            goto FREE_IMAGES;
        }
        case READ_INVALID_BITS: {
            puts("Invalid input data");
            goto FREE_IMAGES;
        }
        case READ_INVALID_HEADER: {
            puts("Input file is not a bmp");
            goto FREE_IMAGES;
        }
        default: {
            puts("Undefined reading error");
            goto FREE_IMAGES;
        }
    }

    out_image = rotate(in_image);
    enum write_status result_to_bmp = to_bmp(output_path, &out_image);
    switch (result_to_bmp) {
        case WRITE_OK: {
            puts("Image is saved");
            failure = 0;
            goto FREE_IMAGES;
        }
        case WRITE_FILENAME_NOT_FOUND: {
            puts("Output file name is not specified");
            goto FREE_IMAGES;
        }
        case WRITE_IMAGE_NOT_FOUND: {
            puts("Output image is null.");
            goto FREE_IMAGES;
        }
        case WRITE_FILE_ERROR: {
            puts("Unable to open output file");
            goto FREE_IMAGES;
        }
        default: {
            puts("Undefined writing error");
            goto FREE_IMAGES;
        }
    }

    FREE_IMAGES:
    image_free(in_image);
    image_free(out_image);
    return failure;
}
