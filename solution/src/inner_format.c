//
// Created by danila on 12/26/21.
//
#include <malloc.h>


#include "inner_format.h"


void image_init(struct image *image, uint64_t height, uint64_t width) {
    image->height = height;
    image->width = width;
    image->data = malloc(height * width * sizeof(struct pixel));
}

struct image image_create(const uint64_t height, const uint64_t width) {
    struct image image;
    image_init(&image, height, width);
    return image;
}


void image_free(const struct image image) {
    free(image.data);
}
