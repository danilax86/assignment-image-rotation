//
// Created by danila on 12/26/21.
//
#include "malloc.h"
#include "transform.h"

struct image rotate(struct image const image) {
    struct image rotated_image = image_create(image.width, image.height);

    for (size_t y = 0; y < image.height; ++y) {
        for (size_t x = 0; x < image.width; ++x) {
            set_pixel(rotated_image,
                      get_pixel(image, x, y),
                      (image.height - 1 - y), x);
        }
    }

    return rotated_image;
}
