//
// Created by danila on 12/26/21.
//

#ifndef INNER_FORMAT_H
#define INNER_FORMAT_H

#include <stdint.h>

struct image {
    uint64_t height, width;
    struct pixel *data;
};

struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};


void image_init(struct image *image, uint64_t height, uint64_t width);

struct image image_create(uint64_t height, uint64_t width);

void image_free(struct image image);

static inline struct pixel get_pixel(const struct image image, const uint64_t x, const uint64_t y) {
    return image.data[x + y * image.width];
}

static inline void set_pixel(struct image image, const struct pixel pixel, const uint64_t x, const uint64_t y) {
    image.data[x + y * image.width] = pixel;
}

#endif //INNER_FORMAT_H
