//
// Created by danila on 12/26/21.
//

#ifndef INPUT_FORMAT_H
#define INPUT_FORMAT_H

#include  <stdio.h>
#include "inner_format.h"

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};


/*  deserializer   */
enum read_status {
    READ_OK = 0,
    READ_FILENAME_NOT_FOUND,
    READ_FILE_ERROR,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_SIGNATURE
};

/*  serializer   */
enum write_status {
    WRITE_OK = 0,
    WRITE_FILE_ERROR,
    WRITE_IMAGE_NOT_FOUND,
    WRITE_FILENAME_NOT_FOUND
};

enum read_status from_bmp(char const *filename, struct image *image);

enum write_status to_bmp(char const *filename, struct image const *image);

#endif //INPUT_FORMAT_H
