//
// Created by danila on 12/26/21.
//

#ifndef TRANSFORM_H
#define TRANSFORM_H

#include "inner_format.h"

struct image rotate(struct image image);

#endif //TRANSFORM_H
